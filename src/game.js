var React = require('react');
var ReactDOM = require('react-dom');
var Field = require('./field.js');
var UI = require('./ui.js');

var Game = React.createClass({

    getInitialState: function() {
        return {
            coord: '(0, 0)'
        }
    },

    ctrlClickHandler: function(coord) {
        this.setState({
            coord: coord
        });
    },
	
	render: function() {
		return( 
	    	<div>
	    		<Field ctrlClickHandler={this.ctrlClickHandler}/>
	    		<UI coord={this.state.coord}/>
	    	</div>
	   	);
    }
});

ReactDOM.render(<Game/>, document.getElementById('game'));