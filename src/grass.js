var React = require('react');
var ReactDOM = require('react-dom');
var Controller = require('./controller.js');

/* Grass simply handles the sprite display of grass tiles.
   TODO: Add pokemon ID and displaying
*/

var Grass = React.createClass({
    getInitialState: function() {
        return {
            rustle: false,
            frame: Math.floor((Math.random() * 6) + 1).toString()
        };
    },
    /* Click to toggle rustle currently not working, probably because Controller is over the sprite
       This is ok though, we don't actually need this.
       I'm considering making Grass a child of Controller.
       Pros: Easier to pass data, makes some sense
       Cons: The controllers for Grass and Pokesquare work similarly, but they (will) handle sprites differently
    */
    handleClick: function(event) {
        this.setState({
            rustle: !this.state.rustle
        });
    },
    render: function() {
        var grassStyle = {};
        grassStyle["zIndex"]= this.props.z.toString();
        grassStyle["left"]= this.props.left.toString()+"px";
        grassStyle["top"]= this.props.top.toString()+"px";
        grassStyle["position"]="absolute";
        if (this.state.rustle) {
            var rustleimg="img/rustlegrass"+this.state.frame+".gif";
            //Controller is slightly offset to make it look better.
            return (
                <div>
                    <img src={rustleimg} style={grassStyle} onClick={this.handleClick}/>
                    <Controller z={this.props.z+1} x={this.props.x} y={this.props.y} left={this.props.left-4} top={this.props.top-4} onClick={this.props.ctrlClickHandler}/>
                </div>
            );
        }
        else {
            return (
                <div>
                    <img src="img/grasstransp.png"  style={grassStyle} onClick={this.handleClick}/>
                    <Controller z={this.props.z+1} x={this.props.x} y={this.props.y} left={this.props.left-4} top={this.props.top-4}  onClick={this.props.ctrlClickHandler}/>
                </div>
            );

        }
    }
});

module.exports = Grass