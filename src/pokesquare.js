var React = require('react');
var ReactDOM = require('react-dom');
var Controller = require('./controller.js');

/* Pokesquare handles the sprite display of middle tiles.
   Will handle user's pokemon and likely pokemon attacks as well.
   TODO: Add pokemon ID and displaying
*/

var Pokesquare = React.createClass({
    getInitialState: function() {
        return {
            occupied: false,
            pokemon: "empty"
        };
    },
    render: function() {
        var pokesquareStyle = {};
        pokesquareStyle["zIndex"]= this.props.z.toString();
        pokesquareStyle["left"]= this.props.left.toString()+"px";
        pokesquareStyle["top"]= this.props.top.toString()+"px";
        pokesquareStyle["position"]="absolute";
        var pokeimage="img/"+this.state.pokemon+".png";
        //Pokemon sprites are 128 by 128 to allow for greater flexibility
        //Controller space is still 64 by 64 for now to avoid overlap with grass controller
        return (
                <div>
                    <img src={pokeimage}  style={pokesquareStyle}/>
                    <Controller z={this.props.z+1} x={this.props.x} y={this.props.y} left={this.props.left+30} top={this.props.top+30} onClick={this.props.ctrlClickHandler}/>
                </div>
            );

        }
    }
);


module.exports = Pokesquare