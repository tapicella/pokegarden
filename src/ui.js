var React = require('react');
var ReactDOM = require('react-dom');

/* The UI class will handle displaying information and toggling user controls */

var UI = React.createClass({
    getInitialState: function() {
        return {
            turn: 0
        };
    },
    render: function() {
        var uiStyle = {};
        uiStyle["background-color"]="#E1E2E3";
        uiStyle["height"]="562px";
        uiStyle["width"]="462px";
        uiStyle["z-index"]= "-1";
        uiStyle["left"]= "562px";
        uiStyle["top"]= "0px";
        uiStyle["position"]= "absolute";

        uiStyle["border-color"]="#D9D9D9";
        uiStyle["border-top-style"]="groove";
        uiStyle["border-right-style"]="groove";
        uiStyle["border-bottom-style"]="groove";
        uiStyle["border-left-style"]="solid";
        return (
                <div style={uiStyle}>{this.props.coord}</div>
            );
    }
});

module.exports = UI