var React = require('react');
var ReactDOM = require('react-dom');

var LikeButton = React.createClass({
  getInitialState: function() {
    return {liked: false};
  },
  handleClick: function(event) {
    this.setState({liked: !this.state.liked});
  },
  render: function() {
    var text = this.state.liked ? 'like' : 'haven\'t liked';
    return (
      <p onClick={this.handleClick}>
        You {text} this. Click to toggle.
      </p>
    );
  }
});

ReactDOM.render(
  <LikeButton />,
  document.getElementById('example1')
);

var Parent = React.createClass({

    getInitialState: function() {
        return {
            value: 'foo'
        }
    },

    changeHandler: function(value) {
        this.setState({
            value: value
        });
    },

    render: function() {
        return (
            <div>
                <Child value={this.state.value} onChange={this.changeHandler} />
                <span>{this.state.value}</span>
                <BaseGrass/>
            </div>
        );
    }
});

var Child = React.createClass({
    propTypes: {
        value: React.PropTypes.string,
        onChange: React.PropTypes.func
    },
    getDefaultProps: function() {
        return {
            value: ''
        };
    },
    changeHandler: function(e) {
        if (typeof this.props.onChange === 'function') {
            this.props.onChange(e.target.value);
        }
    },
    render: function() {
        return (
            <input type="text" value={this.props.value} onChange={this.changeHandler} />
        );
    }
});

var BaseGrass = React.createClass({
    getInitialState: function() {
        return {
            rustle: false
        };
    },
    handleClick: function(event) {
        this.setState({
            rustle: !this.state.rustle
        });
    },
    render: function() {
        if (this.state.rustle) {
            return (
                <img src="img/biggestgrass.gif" onClick={this.handleClick}/>
            );
        }
        else {
            return (
                <img src="img/grasstransp.png" onClick={this.handleClick}/>
            );

        }
    }
});

ReactDOM.render(<Parent/>, document.getElementById('example2'));

