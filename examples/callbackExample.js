var React = require('react');
var ReactDOM = require('react-dom');

var LikeButton = React.createClass({
    displayName: 'LikeButton',

    getInitialState: function () {
        return { liked: false };
    },
    handleClick: function (event) {
        this.setState({ liked: !this.state.liked });
    },
    render: function () {
        var text = this.state.liked ? 'like' : 'haven\'t liked';
        return React.createElement(
            'p',
            { onClick: this.handleClick },
            'You ',
            text,
            ' this. Click to toggle.'
        );
    }
});

ReactDOM.render(React.createElement(LikeButton, null), document.getElementById('example1'));

var Parent = React.createClass({
    displayName: 'Parent',

    getInitialState: function () {
        return {
            value: 'foo'
        };
    },

    changeHandler: function (value) {
        this.setState({
            value: value
        });
    },

    render: function () {
        return React.createElement(
            'div',
            null,
            React.createElement(Child, { value: this.state.value, onChange: this.changeHandler }),
            React.createElement(
                'span',
                null,
                this.state.value
            ),
            React.createElement(BaseGrass, null)
        );
    }
});

var Child = React.createClass({
    displayName: 'Child',

    propTypes: {
        value: React.PropTypes.string,
        onChange: React.PropTypes.func
    },
    getDefaultProps: function () {
        return {
            value: ''
        };
    },
    changeHandler: function (e) {
        if (typeof this.props.onChange === 'function') {
            this.props.onChange(e.target.value);
        }
    },
    render: function () {
        return React.createElement('input', { type: 'text', value: this.props.value, onChange: this.changeHandler });
    }
});

var BaseGrass = React.createClass({
    displayName: 'BaseGrass',

    getInitialState: function () {
        return {
            rustle: false
        };
    },
    handleClick: function (event) {
        this.setState({
            rustle: !this.state.rustle
        });
    },
    render: function () {
        if (this.state.rustle) {
            return React.createElement('img', { src: 'img/biggestgrass.gif', onClick: this.handleClick });
        } else {
            return React.createElement('img', { src: 'img/grasstransp.png', onClick: this.handleClick });
        }
    }
});

ReactDOM.render(React.createElement(Parent, null), document.getElementById('example2'));