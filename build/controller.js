var React = require('react');
var ReactDOM = require('react-dom');

/* This class controls interaction with a square
   It sends (will send) data back up to Field and Game upon user interaction
   It is the functional counterpart to Grass and Pokesquare, which handle visuals
*/

var Controller = React.createClass({
    displayName: 'Controller',

    getInitialState: function () {
        return {
            //These ae just possible state variables, control code not implemented yet
            occupied: false,
            selected: false,
            type: null
        };
    },

    clickHandler: function (e) {
        this.state.selected = !this.state.selected;
        var coord = "(" + this.props.x.toString() + ", " + this.props.y.toString() + ")";
        this.props.onClick(coord);
    },
    render: function () {
        var controllerStyle = {};
        controllerStyle["zIndex"] = this.props.z.toString();
        controllerStyle["left"] = (this.props.left + 12).toString() + "px";
        controllerStyle["top"] = (this.props.top + 10).toString() + "px";
        controllerStyle["position"] = "absolute";
        controllerStyle["width"] = "48px";
        controllerStyle["height"] = "48px";
        if (this.state.selected) {
            return React.createElement('div', { style: controllerStyle, className: 'selectclass', onClick: this.clickHandler });
        } else {
            return React.createElement('div', { style: controllerStyle, className: 'hoverclass', onClick: this.clickHandler });
        }
    }
});

module.exports = Controller;