var React = require('react');
var ReactDOM = require('react-dom');
var Field = require('./field.js');
var UI = require('./ui.js');

var Game = React.createClass({
    displayName: 'Game',

    getInitialState: function () {
        return {
            coord: '(0, 0)'
        };
    },

    ctrlClickHandler: function (coord) {
        this.setState({
            coord: coord
        });
    },

    render: function () {
        return React.createElement(
            'div',
            null,
            React.createElement(Field, { ctrlClickHandler: this.ctrlClickHandler }),
            React.createElement(UI, { coord: this.state.coord })
        );
    }
});

ReactDOM.render(React.createElement(Game, null), document.getElementById('game'));