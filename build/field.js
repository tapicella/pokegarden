var React = require('react');
var ReactDOM = require('react-dom');
var Grass = require('./grass.js');
var Pokesquare = require('./pokesquare.js');

/* Field controls the pokemon and grass gameplay zone
   It sets up both the visuals and the interactive structures
   TODO: Field will also help pass data up from the control structures to Game
*/

var Field = React.createClass({
    displayName: 'Field',

    /*
    Personal reminders on how to use these functions
      getInitialState: function() {
        return {
            value: 'foo'
        }
    },
      changeHandler: function(value) {
        this.setState({
            value: value
        });
    },*/

    render: function () {
        var fieldStyle = {};
        fieldStyle["background-color"] = "#00f88b";
        fieldStyle["width"] = "562px";
        fieldStyle["height"] = "562px";
        fieldStyle["position"] = "absolute";
        fieldStyle["z-index"] = "-1";
        fieldStyle["left"] = "0px";
        fieldStyle["top"] = "0px";

        fieldStyle["border-color"] = "#02ED86";
        fieldStyle["border-top-style"] = "groove";
        fieldStyle["border-right-style"] = "none";
        fieldStyle["border-bottom-style"] = "groove";
        fieldStyle["border-left-style"] = "groove";

        /*Grasses are the grass tiles
          Squares are the spaces in the center of every 4 grass tiles
          TODO: Add coord IDs to both tile types.
          Grass will be evens (0,2), (4,4), etc
          Squares will be odds
          e.g. (1,1) will be the first square at the center of (0,0) (0,2) (2,0) (2,2)
          This preserves a regular grid numbering system while allowing for the staggering effect
        */

        var grasses = [];
        var squares = [];
        for (var i = 0; i < 8; i++) {
            for (var j = 0; j < 8; j++) {
                var z = i;
                var left = j * 70 + 2;
                var top = i * 70 + 2;
                //Grasses are an 8x8 grid, squares are 7x7 within that grid
                grasses.push(React.createElement(Grass, { z: z, x: 2 * j, y: 2 * i, left: left, top: top, ctrlClickHandler: this.props.ctrlClickHandler }));
                if (j !== 7 && i !== 7) squares.push(React.createElement(Pokesquare, { z: z, x: 2 * j + 1, y: 2 * i + 1, left: left, top: top, ctrlClickHandler: this.props.ctrlClickHandler }));
            }
        }

        return React.createElement(
            'div',
            { style: fieldStyle },
            grasses,
            squares
        );
    }
});

module.exports = Field;